package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
	
	static int maxCapacity = 20;
	
	List<FridgeItem> items = new ArrayList<FridgeItem>();

	/**
	 * Returns the number of items currently in the fridge
	 * 
	 * @return number of items in the fridge
	 */
	@Override
	public int nItemsInFridge() {
		int nItems = this.items.size();
		return nItems;
	}

	/**
	 * The fridge has a fixed (final) max size.
	 * Returns the total number of items there is space for in the fridge
	 * 
	 * @return total size of the fridge
	 */
	@Override
	public int totalSize() {
		return maxCapacity;
	}
	
	/**
	 * Returns the remaining number of items there is space for in the fridge
	 * 
	 * @return number of remaining spaces in fridge
	 */
	private int remainingSpace() {
		int nItems = this.nItemsInFridge();
		int maxItems = this.totalSize();
		int remainingCapacity = maxItems - nItems;
		return remainingCapacity;
	}

	/**
	 * Place a food item in the fridge. Items can only be placed in the fridge if
	 * there is space
	 * 
	 * @param item to be placed
	 * @return true if the item was placed in the fridge, false if not
	 */
	@Override
	public boolean placeIn(FridgeItem item) {
		int remainingCapacity = this.remainingSpace();
		if (remainingCapacity <= 0) {
			return false;
		}
		else {
			this.items.add(item);
			return true;
		}
	}

	/**
	 * Remove item from fridge
	 * 
	 * @param item to be removed
	 * @throws NoSuchElementException if fridge does not contain <code>item</code>
	 */
	@Override
	public void takeOut(FridgeItem item) {
		if (this.items.contains(item)) {
			this.items.remove(item);
		}
		else {
			throw new NoSuchElementException();
		}

	}

	/**
	 * Remove all items from the fridge
	 */
	@Override
	public void emptyFridge() {
		this.items.clear();

	}

	/**
	 * Remove all items that have expired from the fridge
	 * @return a list of all expired items
	 */
	@Override
	public List<FridgeItem> removeExpiredFood() {
		List<FridgeItem> expired = new ArrayList<FridgeItem>();
		
		// I decided to do a decrementing for loop instead of an incrementing one because I imagined problems would arise otherwise as later objects gets moved forward.
		for (int i = (nItemsInFridge() - 1); i >= 0; i--) {
			FridgeItem item = this.items.get(i);
			if (item.hasExpired()) {
				expired.add(item);
				this.items.remove(item);
			}
		}
		
		return expired;
	}

}
